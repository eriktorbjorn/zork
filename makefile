SUBDIRS=gobj_parse func_dump mdlfun

.PHONY: subdirs $(SUBDIRS)

all: mdlfun

clean:
	$(MAKE) clean -C gobj_parse
	$(MAKE) clean -C func_dump
	$(MAKE) clean -C mdlfun

gobj_parse:
	$(MAKE) -C $@

func_dump: gobj_parse
	$(MAKE) -C $@

mdlfun: func_dump
	$(MAKE) -C $@


