// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
#include <iostream>

// Headers for CppUnitTest
#include "CppUnitTest.h"

template <typename T, size_t sz>
size_t ARRSIZE(const T(&)[sz])
{
    return sz;
}
#define NOT_IMPL std::cerr << __FUNCTION__ << " is not implemented.\n"; _ASSERT(0); return false;
#define NOT_IMPL_RM std::cerr << __FUNCTION__ << " is not implemented.\n"; _ASSERT(0); return RoomP();
#define NIFN(name) bool name() { NOT_IMPL; }
#define NIFN_RM(name) std::any name() { NOT_IMPL_RM;};
