#include "stdafx.h"
#include <algorithm>
#include "CppUnitTest.h"
#include "object.h"
#include "act1.h"
#include "dung.h"
#include "funcs.h"
#include "util.h"
#include "makstr.h"
#include "rooms.h"
#include "objfns.h"
#include "parser.h"
#include <time.h>
#include "room.h"
#include "globals.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{		
    TEST_CLASS(UnitTest1)
    {
    public:

        TEST_CLASS_INITIALIZE(TestClassInit)
        {
            srand((unsigned int)time(NULL));
            init_dung();
        }

        TEST_METHOD_INITIALIZE(TestMethodInit)
        {
        }

        TEST_METHOD(TestTreas)
        {
            here = sfind_room("WHOUS");
            RoomP old_here = here;
            winner = player();
            prsvec[0] = find_verb("TREAS");
            treas();
            Assert::IsTrue(old_here == here, L"TREAS moves when it shouldn't have.");
            goto_(sfind_room("TEMP1"));
            treas();
            Assert::IsTrue(here == sfind_room("TREAS"), L"TREAS did not move to treasure room.");
            prsvec[0] = find_verb("TEMPL");
            treas();
            Assert::IsTrue(here == sfind_room("TEMP1"), L"TREAS did not move to temple.");
        }

        TEST_METHOD(TestScore)
        {
            // The diamond should have an ofval of 10 and an otval of 6.
            auto o = get_obj("DIAMO");
            Assert::IsNotNull(o.get(), L"Can't get the diamond object");
            Assert::IsTrue(o->ofval() == 10, L"ofval is incorrect");
            Assert::IsTrue(o->otval() == 6, L"otval is incorrect.");

            auto &gobj = global_objects();
            int ofval = 0;
            int otval = 0;
            int ofval_count = 0;
            for each (auto &go in gobj)
            {
                ofval += go->ofval();
                if (go->ofval() != 0)
                {
                    ofval_count++;
                }
                otval += go->otval();
            }
            auto &ob = object_pobl();
            for each (auto &o in ob)
            {
                ofval += o.second->ofval();
                if (o.second->ofval() != 0)
                {
                    ofval_count++;
                }
                otval += o.second->otval();
            }
            Assert::IsTrue(ofval == 255, L"ofval is incorrect");
            Assert::IsTrue(ofval_count == 31, L"Incorrect number of ofval objects");
            Assert::IsTrue(otval == 236, L"otval is incorrect");
        }

        TEST_METHOD(TestGetObj)
        {
            auto o = get_obj("BROPE");
            Assert::IsNotNull(o.get(), L"get_obj returned null");
            auto l = get_obj("BLAMP");
            Assert::IsNotNull(l.get(), L"get_obj didn't get blamp");
            Assert::IsTrue(o->oid() != l->oid(), L"get_obj did not return distinct objects");
        }

        TEST_METHOD(TestLateInit)
        {
            auto c = get_obj("CROWN");
            Assert::IsTrue(c->oid() == "CROWN", L"Did not get crown object");
            Assert::IsTrue(c->oadjs()[0] == "GAUDY", L"Late init object is not correct.");
            // Make sure this object is contained in the safe on init.
            auto s = get_obj("SAFE");
            Assert::IsTrue(s->oid() == "SAFE", L"Could not get safe");
            auto contents = s->ocontents();
            auto loc = std::find_if(contents.begin(), contents.end(), [](ObjectP p)
            {
                return p->oid() == "CROWN";
            });
            Assert::IsTrue(loc != contents.end(), L"CROWN is not in the SAFE");
            // Make sure the two crown objects really point to the same thing.
            Assert::IsTrue(c.get() == (*loc).get(), L"Two CROWNS do not match");
        }

        TEST_METHOD(TestFrontDoor)
        {
            auto door = get_obj("FDOOR");
            Assert::IsNotNull(door.get(), L"Unable to get door object");
            prsvec[0] = find_verb("OPEN");
            Assert::IsTrue(door->call_fn(), L"OPEN did not return true");
            prsvec[0] = find_verb("BURN");
            Assert::IsTrue(door->call_fn(), L"BURN did not return true");
            prsvec[0] = find_verb("MUNG");
            Assert::IsTrue(door->call_fn(), L"MUNG did not return true");
            prsvec[0] = find_verb("TAKE");
            Assert::IsFalse(door->call_fn(), L"TAKE did not return false");
        }

        TEST_METHOD(TestMaxScore)
        {
            // Make score of this game should be 616 points.
            Assert::IsTrue(score_max() == 616, L"Max score is incorrect.");
        }

        TEST_METHOD(TestRglobal)
        {
            RoomP house = sfind_room("WHOUS");
            Assert::IsTrue(rtrnn(house, housebit), L"Housebit not set in globals");
        }

        TEST_METHOD(TestSpliceOut)
        {
            RoomP house = sfind_room("WHOUS");
            Assert::IsTrue(house->robjs().size() > 0, L"House does not have any objects.");
            // Splice out an object that doesn't exist.
            house->robjs() = splice_out(sfind_obj("CBAG"), house->robjs());
            Assert::IsTrue(house->robjs().size() == 3, L"splice_out removed an invalid object?");
            house->robjs() = splice_out(sfind_obj("MAILB"), house->robjs());
            Assert::IsTrue(house->robjs().size() == 2, L"Mailbox was not removed.");
            // Restore the mailbox.
            house->robjs().push_back(sfind_obj("MAILB"));
        }

        TEST_METHOD(TestObjectRoomLocs)
        {
            // Tests that all objecs in a room have their location set to that room.
            auto &rm = rooms();
            for each(auto rp in rm)
            {
                for each (ObjectP op in rp.second->robjs())
                {
                    wchar_t objs[64];
                    size_t conv;
                    mbstowcs_s(&conv, objs, op->oid().c_str(), 64);
                    wstringstream wss;
                    wss << objs << L" not in room ";
                    mbstowcs_s(&conv, objs, rp.second->rid().c_str(), 64);
                    wss << objs;
                    // Special case some objects in multiple locations.
                    if (op->oid() == "ODOOR" || op->oid() == "QDOOR" ||
                        op->oid() == "CCLIF" || op->oid() == "GRATE" ||
                        op->oid() == "WINDO" || op->oid() == "RAINB" ||
                        op->oid() == "DOOR" || op->oid() == "CDOOR" ||
                        op->oid() == "PDOOR" || op->oid() == "PWIND" ||
                        op->oid() == "TOMB" || op->oid() == "WCLIF")
                    {
                        continue;
                    }
                    Assert::IsTrue(op->oroom() == rp.second, wss.str().c_str());
                }
            }
        }

        TEST_METHOD(TestObjectContainment)
        {
            auto &objs = object_pobl();
            for each (auto &op in objs)
            {
                for each (auto c in op.second->ocontents())
                {
                    Assert::IsTrue(c->ocan() != nullptr, L"Object not set correctly.");
                    stringstream ss;
                    ss << c->ocan()->oid() << " not in " << op.second->oid();
                    wchar_t buff[128];
                    size_t conv;
                    mbstowcs_s(&conv, buff, ss.str().c_str(), 128);
                    Assert::IsTrue(c->ocan() == op.second, buff);
                }
            }
        }

        TEST_METHOD(TestUnimpl)
        {
            Assert::IsTrue(do_script(), L"Script needs to be implemented.");
            Assert::IsTrue(do_unscript(), L"Unscript needs to be implemented.");
            Assert::IsTrue(do_save(), L"Save needs to be implemented.");
            Assert::IsTrue(finish(), L"finish needs to be implemented.");
            Assert::IsTrue(do_restore(), L"Restore needs to be implemented.");
            Assert::IsTrue(restart(), L"Restart needs to be implemented.");
            Assert::IsTrue(feech(), L"Feech needs to be implemented.");
        }

        TEST_METHOD(TestVerbose)
        {
            Assert::IsTrue(false, L"Test verbose -- something's wrong with it.");
        }

        TEST_METHOD(TestAdjectives)
        {
            for each (auto iter in object_pobl())
            {
                // ObjectP is in the second item.
                auto &adjs = iter.second->oadjs();
                for each(auto &name in adjs)
                {
                    AdjectiveP a = find_adj(name);
                    Assert::IsTrue((bool)a, L"Can't get adjective.");
                }
            }
        }

        TEST_METHOD(TestWait)
        {
            int old_moves = moves;
            wait();
            Assert::IsTrue(old_moves == moves - 3, L"Wait did not wait.");
        }

        TEST_METHOD(TestGetObject)
        {
            // We're in the WHOUS room right now.
            save_it(true);
            Assert::IsTrue(get_object("CARD", AdjectiveP()) == Nefals(), L"Missing object did not return the right value");
            Assert::IsTrue(get_object("MAT", AdjectiveP()) == Nefals(sfind_obj("MAT"), 0), L"Existing object did not get returned.");

            // Move to the kitchen
            here = sfind_room("KITCH");
            Assert::IsTrue(get_object("FOOD", AdjectiveP()) == Nefals(), L"Can't find the food in the bag.");
            // Open the bag
            tro(sfind_obj("SBAG"), openbit);
            // Make sure the food is visible.
            Assert::IsTrue(get_object("FOOD", AdjectiveP()) == Nefals(sfind_obj("FOOD"), 0), L"Can't find the food in the bag.");

            // Check adjectives.
            here = sfind_room("ALICE");
            rtro(here, rlightbit);
            Assert::IsTrue(get_object("CAKE", AdjectiveP()) == Nefals(ObjectP(), 1), L"Getting a cake did not return ambiguous.");
            Assert::IsTrue(get_object("RDICE", AdjectiveP()) == Nefals(sfind_obj("RDICE"), 0), L"RDICE failed");
            Assert::IsTrue(get_object("CAKE", find_adj("RED")) == Nefals(sfind_obj("RDICE"), 0), L"CAKE with RED failed.");
        }

        TEST_METHOD(TestSearchList)
        {
            auto room = sfind_room("WHOUS");
            Assert::IsNotNull(room.get(), L"Can't get room.");
            auto result = search_list("MAILB", room->robjs(), nullptr);
            Assert::IsTrue(get<0>(result) != ObjectP(), L"Didn't find mailbox.");
            result = search_list("CORPS", room->robjs(), nullptr);
            Assert::IsTrue(get<0>(result) == ObjectP() && get<1>(result) == 0, L"Missing object did not return error.");

            // Ambiguous
            room = sfind_room("ALICE");
            Assert::IsNotNull(room.get(), L"Can't get room.");
            result = search_list("CAKE", room->robjs(), nullptr);
            Assert::IsTrue(result == nefals, L"Ambiguous object did not return nefals.");
            result = search_list("CAKE", room->robjs(), find_adj("RED"));
            Assert::IsTrue(get<0>(result) == sfind_obj("RDICE"), L"Red cake search returned the wrong object.");
        }

        TEST_METHOD(TestLookInside)
        {
            save_it(true);
            auto bag = sfind_obj("SBAG");
            Assert::IsTrue((bool) bag, L"Can't get bag");
            prsvec[1] = bag;
            look_inside();
            tro(bag, openbit);
            look_inside();
        }

        TEST_METHOD(TestAxe)
        {
            auto axe = get_obj("AXE");
            Assert::IsNotNull(axe.get(), L"Unable to get AXE");
            prsvec[0] = find_verb("CLOSE");
            Assert::IsFalse(axe->call_fn(), L"AXE did not properly return false");
            prsvec[0] = find_verb("TAKE");
            Assert::IsTrue(axe->call_fn(), L"Take Axe did not succeed");
        }

        TEST_METHOD(TestRoomContents)
        {
            RoomP whous = get_room("WHOUS");
            Assert::IsTrue(whous != nullptr, L"Can't get house");
            auto contents = whous->robjs();
            Assert::IsTrue(contents.size() == 3, L"WHOUS should have 3 objects on startup");
            const char *objs[] = { "FDOOR", "MAILB", "MAT" };
            for each(auto o in objs)
            {
                Assert::IsTrue(std::find_if(contents.begin(), contents.end(), [o](ObjectP p)
                {
                    return p->oid() == o;
                }) != contents.end(), L"Objects are not correct in house");
            }

            RoomP spal = get_room("SPAL");
            Assert::IsTrue(spal != nullptr, L"Can't get SPAL");
            contents = spal->robjs();
            Assert::IsTrue(contents.size() == 2, L"SPAL has the wrong number of objects");
            const char *objs2[] = { "PAL3", "STOVE" };
            for each(auto o in objs2)
            {
                Assert::IsTrue(std::find_if(contents.begin(), contents.end(), [o](ObjectP p)
                {
                    return p->oid() == o;
                }) != contents.end(), L"Objects are not correct in sooty room");
            }
        }

        TEST_METHOD(TestWindowFunction)
        {
            prsvec[0] = find_verb("OPEN");
            Assert::IsTrue(!trnn(sfind_obj("WINDO"), openbit), L"Window is not closed.");
            obj_funcs::window_function();
            Assert::IsTrue(trnn(sfind_obj("WINDO"), openbit), L"Window is not opened.");
            obj_funcs::window_function();
            Assert::IsTrue(trnn(sfind_obj("WINDO"), openbit), L"Window is not opened.");
            prsvec[0] = find_verb("CLOSE");
            obj_funcs::window_function();
            Assert::IsTrue(!trnn(sfind_obj("WINDO"), openbit), L"Window is not re-closed.");
        }

        TEST_METHOD(TestFallsRoom)
        {
            save_it();
            here = sfind_room("FALLS");
            Assert::IsTrue(perform(here->raction(), find_verb("LOOK")), L"falls room did not return true");
        }

        TEST_METHOD(TestExits)
        {
            auto &rm = rooms();
            for each (auto i in rm)
            {
                auto &exits = i.second->rexits();
                for each (auto ex in exits)
                {
                    try
                    {
                        auto s = std::any_cast<const char*>(get<1>(ex));
                        RoomP rexit = find_room(s);
                        Assert::IsTrue(rexit.get() != nullptr, L"Room exit does not exist.");
                    }
                    catch (std::bad_any_cast &)
                    {

                    }
                }
            }
        }

        TEST_METHOD(TestIterator)
        {
            typedef std::list<int> IntList;
            std::list<int> test_list{ 1,2,3,4,5,6,7 };
            auto iter = Iterator<IntList>(test_list);
            auto iter2 = rest(iter, 3);
            Assert::IsTrue(*iter2 == 4, L"rest did not work");

            // Advance past the end -- should throw.
            Assert::ExpectException<ZorkException>([iter]() { rest(iter, 8); }, L"Advancing out of list did not throw.");
            // Advance before the beginning.
            Assert::ExpectException<ZorkException>([iter2]() { back(iter2, 4); }, L"Back did not throw properly.");

            std::string s = "This is a string.";
            auto siter = Iterator<std::string>(s);
            auto siter2 = rest(siter, 8);
            Assert::IsTrue(length(siter) == length(siter2) + 8, L"Length (or rest) did not work.");
        }

	};
}