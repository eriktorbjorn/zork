// func_dump.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <boost/program_options.hpp>
#include <string>
#include <fstream>
#include <iostream>
#include <set>

using namespace std;
namespace po = boost::program_options;

void process_file(const string &filename, set<string> &funcs, const string &hdr)
{
    ifstream ifs(filename);
    string line;
    while (getline(ifs, line))
    {
        size_t off = 0;
        while ((off = line.find(hdr, off)) != string::npos)
        {
            off += hdr.size() + 2; // Skip to after the colons
            size_t endp = off;
            while (isalnum(line[endp]) || line[endp] == '_')
            {
                ++endp;
            }
            funcs.insert(line.substr(off, endp - off));
        }
    }
}

int main(int argc, char *argv[])
{
    po::options_description desc("Options");
    po::variables_map vm;
    string filename[2];
    bool rooms;

    desc.add_options()
        ("input_file1,i", po::value<string>(&filename[0])->required(), "Input file 1")
        ("input_file2,j", po::value<string>(&filename[1]), "Input file 2 (optional)")
        ("rooms,r", po::value<bool>(&rooms)->default_value(false), "rooms or objects");

    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    }
    catch (exception &e)
    {
        cerr << e.what() << endl;
        cerr << desc << endl;
        return 1;
    }

    vector<string> hdrs;
    if (rooms)
    {
        hdrs = { "room_funcs", "exit_funcs" };
    }
    else
        hdrs = { "obj_funcs" };

    cout << "#pragma once\n\n";
    cout << "// This header is auto-generated. Do not edit." << endl;
    cout << "#include \"defs.h\"" << endl;

    for (const string &hdr : hdrs)
    {
        cout << "namespace " << hdr << " {" << endl;
        set<string> funcs;
        process_file(filename[0], funcs, hdr);
        if (!filename[1].empty())
        {
            process_file(filename[1], funcs, hdr);
        }


        // Add a couple more that are precompiled macros.
        if (rooms && hdr == "exit_funcs")
        {
            funcs.insert("magnet_room_exit");
            funcs.insert("mrgo");
            funcs.insert("mirin");
            funcs.insert("mirout");
            funcs.insert("maybe_door");
        }

        string return_type = (hdr == "exit_funcs") ? "ExitFuncVal" : "bool";

        for (auto &s : funcs)
        {
            // Exit functions return a RoomP.
            cout << "    " << return_type << " " << s << "();" << endl;
        }

        cout << "}\n\n";
    }

    return 0;
}

