@echo off

msbuild zork_cpp.sln /t:build /p:Platform=x86 /p:Configuration=Release  /m
if errorlevel 1 goto FAILED
msbuild zork_cpp.sln /t:build /p:Platform=x64 /p:Configuration=Release  /m
if errorlevel 1 goto FAILED

goto DONE

:FAILED
echo Build failed.
goto DONE

:DONE
