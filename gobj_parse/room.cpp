#include "stdafx.h"
#include "funcs.h"
#include <iostream>

using namespace std;

size_t dump_string(size_t pos, const string &mdl, ostream &os, const std::string &tail)
{
    if (mdl[pos] == '\"')
    {
        bool end_found = false;
        size_t end = pos + 1;
        string prop = "\"";
        while (!end_found)
        {
            auto temp = mdl.find_first_of("\"\n\r", end);
            prop += mdl.substr(end, temp - end);
            if (mdl[temp] == '\n')
                prop += "\\n";
            end = temp;
            if (mdl[end] == '\"' && mdl[end - 1] == '\\')
            {
                prop += "\"";
            }
            else if (mdl[end] == '\"')
            {
                end_found = true;
            } 
            ++end;
        }
        prop += "\"";
        os << prop;
        return end;
    }
    else if (mdl[pos] == ',')
    {
        auto end = mdl.find_first_of(">\n\r ", pos);
        auto prop = mdl.substr(pos + 1, end - pos - 1);
        reduce(prop);
        os << prop << tail;
        return end;
    }
    else
        throw runtime_error("Not a room description");
}

size_t dump_door(size_t pos, const std::string &mdl, ostream &os)
{
    os << "std::make_shared<DoorExit>(";
    // First three (required) are strings.
    pos = passws(pos, mdl);
    pos = dump_string(pos, mdl, os);
    os << ", ";
    pos = passws(pos, mdl);
    pos = dump_string(pos, mdl, os);
    os << ", ";
    pos = passws(pos, mdl);
    pos = dump_string(pos, mdl, os);

    pos = passws(pos, mdl);
    if (mdl[pos] == '>')
    {
        // Nothing left to do.
        // Just increment pos to pass the end of
        // the door definition.
        ++pos;
    }
    else
    {
        os << ", ";
        pos = dump_string(pos, mdl, os);
        pos++;
    }
    os << ")";
    return pos;
}

size_t dump_cexit(size_t pos, const std::string &mdl, ostream &os)
{
    os << "std::make_shared<CExit>(";
    pos = passws(pos, mdl);
    if (mdl[pos] != '\"')
        throw runtime_error("Invalid cexit");
    pos++;
    auto end = mdl.find_first_of('\"', pos);
    auto prop = mdl.substr(pos, end - pos);
    reduce(prop);
    os << prop << ", ";
    pos = mdl.find_first_of('\"', end + 1);
    end = mdl.find_first_of('\"', pos + 1);
    prop = mdl.substr(pos, end - pos + 1);
    os << prop << ", ";
    // There may or may not be a description.
    pos = mdl.find_first_of("\">,", end + 1);
    if (mdl[pos] == '>')
    {
        // No description.
        os << "\"\"";
        // Advance past the description's '>'
        pos = end + 1;
    }
    else if (mdl[pos] == ',')
    {
        end = mdl.find_first_of("> \n\r", pos);
        prop = mdl.substr(pos + 1, end - pos - 1);
        reduce(prop);
        os << prop;
        pos = end;
    }
    else
    {
        // Error message on exit failure
        end = mdl.find_first_of('\"', pos + 1);
        os << mdl.substr(pos, end - pos + 1);
        pos = end + 1;
    }
    // Description (if any) is done.
    pos = passws(pos, mdl);
    if (mdl[pos] != '>')
    {
        if (mdl[pos] == '<')
        {
            // Empty flag
            os << ", false";
            pos = passws(pos + 2, mdl);
        }
        else if (mdl[pos] == '>')
        {
            // Done
        }
        else if (mdl[pos] == 'T')
        {
            os << ", true";
            pos = passws(pos + 1, mdl);
        }
        else {
            _ASSERT(0);
        }
        if (mdl[pos] != '>')
        {
            // Function
            end = mdl.find_first_of('>', pos);
            prop = mdl.substr(pos, end - pos);
            reduce(prop);
            os << ", exit_funcs::" << prop;
        }
    // Move to the end of the form.
        pos = mdl.find_first_of('>', end);
    }
    os << ")";
    // pos is now at the terminating >, so increment
    ++pos;
    return pos;
}

size_t dump_setg(size_t pos, const std::string &mdl, ostream &os)
{
    os << "std::make_shared<SetgExit>(";
    pos = passws(pos, mdl);
    auto end = skiptows(pos, mdl);
    auto prop = mdl.substr(pos, end - pos);
    reduce(prop);
    os << "\"" << prop << "\", ";
    pos = passws(end + 1, mdl);

    if (mdl.compare(pos, 6, "<CEXIT") == 0)
    {
        pos = dump_cexit(pos + 6, mdl, os);
        os << ")";
    }
    else
    {
        _ASSERT(0);
    }
    return pos;
}

size_t dump_exits(const std::string &rmid, size_t pos, const std::string &mdl, ostream &os)
{
    // No exits?
    if (mdl.compare(pos, 8, ",NULEXIT") == 0)
    {
        os << "{ }";
        return pos + 8;
    }
    // Must be at a <
    if (mdl.compare(pos, 6, "<EXIT ") != 0)
    {
        throw runtime_error("Not at an exit");
    }
    os << "{ ";
    pos += 6; // First exit.
    bool done = false;
    bool first = true;
    while (mdl[pos] != '>')
    {
        pos = passws(pos, mdl);
        if (mdl[pos] != '\"')
            throw runtime_error("Invalid exit format");
        if (!first)
            os << ", ";
        first = false;
        ++pos;
        auto end = mdl.find_first_of('\"', pos);
        auto prop = mdl.substr(pos, end - pos);
        reduce(prop);
        prop[0] = toupper(prop[0]);
        os << "Ex(";
        if (prop == "#!#!#")
        {
            os << "NullExit";
        }
        else
        {
            os << prop;
        }
        os << ", ";
        pos = passws(end + 1, mdl);
        // If the character is a comma, just output the raw text.
        if (mdl[pos] == ',')
        {
            pos = dump_string(pos, mdl, os, "_" + rmid);
        }
        else if (mdl.compare(pos, 6, "#NEXIT") == 0)
        {
            pos += 6;
            pos = passws(pos, mdl);
            // Kludge fix for duplicate #NEXIT in dung.mud.
            if (mdl.compare(pos, 6, "#NEXIT") == 0)
            {
                pos += 6;
                pos = passws(pos, mdl);
            }
            os << "NExit(";
            pos = dump_string(pos, mdl, os);
            os << ")";
        }
        else if (mdl.compare(pos, 6, "<CEXIT") == 0)
        {
            pos = dump_cexit(pos + 6, mdl, os);
        }
        else if (mdl.compare(pos, 5, "<DOOR") == 0)
        {
            pos = dump_door(pos + 5, mdl, os);
        }
        else if (mdl.compare(pos, 5, "<SETG") == 0)
        {
            pos = dump_setg(pos + 5, mdl, os);
        }
        else
        {
            pos = dump_string(pos, mdl, os);
        }
        os << ")";
        pos = passws(pos, mdl);
    }
    os << " }";
    return pos + 1;
}

void proc_room(const std::string &mdl, ostream &os)
{
    int room_count = 0;
    string::size_type pos = 0;
    while ((pos = mdl.find("<ROOM ", pos)) != string::npos)
    {
        // Skip past the room header
        pos += 5;
        os << "mr(";
        pos = passws(pos, mdl);
        bool timber_room = false;
        if (mdl.compare(pos, 7, "\"TIMBE\"") == 0)
        {
            timber_room = true;
        }
        if (mdl[pos] != '\"')
            throw std::runtime_error("Room does not start with a quote.");
        std::string rmid = mdl.substr(pos + 1, mdl.find_first_of('\"', pos + 1) - (pos + 1));
        pos = dump_string(pos, mdl, os);
        os << ", ";
        // Description 1
        pos = passws(pos, mdl);
        pos = dump_string(pos, mdl, os);
        os << ", ";
        // Description 2
        pos = passws(pos, mdl);
        pos = dump_string(pos, mdl, os);
        os << ", ";

        pos = passws(pos, mdl);
        pos = dump_exits(rmid, pos, mdl, os);
        os << ", ";

        // Special case for timber room, since I'm too tired to
        // handle it properly. This parser makes a mistake when
        // the global value is set during the exits.
        if (timber_room)
            ++pos;

        pos = dump_contents(passws(pos, mdl), mdl, os);
        pos = passws(pos, mdl);
        if (mdl[pos] == '>')
        {
            // end of object..just quit.
        }
        else
        {
            os << ", ";
            pos = dump_func(pos, mdl, "room_funcs", os);
            pos = passws(pos, mdl);
            if (mdl[pos] != '>')
            {
                os << ", ";
                pos = passws(add_bits(pos, mdl, os), mdl);
                if (mdl[pos] != '>')
                {
                    os << ", ";
                    pos = dump_props(pos, mdl, "RP", os);
                }
            }
        }

        ++pos;
        ++room_count;
        os << "),\n";
    }
    cerr << "Found " << room_count << " rooms\n";
}