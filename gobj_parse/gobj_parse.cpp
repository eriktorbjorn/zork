// gobj_parse.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <boost/program_options.hpp>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <string>
#include <ctime>
#include <iomanip>
#include <algorithm>
#include "funcs.h"

using namespace std;
namespace po = boost::program_options;

char convert(char c)
{
    c = tolower(c);
    if (c == '-')
        c = '_';
    if (c == '&')
        c = '_';
    return c;
}


size_t passws(size_t start, const string &s)
{
    while (s[start] == ' ' || s[start] == '\r' || s[start] == '\n' || s[start] == '\t')
        ++start;
    return start;
}

size_t skiptows(size_t start, const string &s)
{
    while (s[start] != ' ' && s[start] != '\r' && s[start] != '\n' && s[start] != '\t')
        ++start;
    return start;
}

size_t dump_bracket_list(size_t p, const string &mdl, ostream &os)
{
    bool first = true;
    os << "{ ";
    p = mdl.find('[', p) + 1;
    while ((p = mdl.find_first_of("]\"", p)) != string::npos)
    {
        if (mdl[p] == ']')
            break;
        if (!first)
        {
            os << ", ";
        }
        first = false;
        size_t end = mdl.find('\"', p + 1);
        os << mdl.substr(p, end - p + 1);
        p = end + 1;
    }
    os << " }, ";
    return p+1;
}

size_t add_bits(size_t p, const std::string &mdl, ostream &os)
{
    // Bits starts with a <+, or can be 0 if empty.
    p = mdl.find_first_of("<0,", p);
    if (mdl[p] == '0')
    {
        // No flags. Output an empty initializer list and return.
        os << "{ }";
        return p + 1;
    }
    else if (mdl[p] == ',') // Single flag
    {
        auto end = mdl.find_first_of("> \n\r", p);
        auto flag = mdl.substr(p + 1, end - p - 1);
        transform(flag.begin(), flag.end(), flag.begin(), convert);
        os << "{ " << flag << "} ";
        end = mdl.find_first_of(" \n\r>", end);
        //end = skiptows(end, mdl);
        return end;
    }
    p = mdl.find("<+", p);
    os << "{ ";
    bool first = true;
    while ((p = mdl.find_first_of(">,", p)) != string::npos)
    {
        if (mdl[p] == '>')
            break;
        if (!first)
            os << ", ";
        first = false;
        p++; // Skip the comma
        auto end = mdl.find_first_of(" ,>\n\r", p);
        string flag = mdl.substr(p, end - p);
        transform(flag.begin(), flag.end(), flag.begin(), convert);
        os << flag;
    }
    os << " } ";
    return p + 1;
}

size_t dump_func(size_t p, const string &mdl, const std::string &nspace, ostream &os)
{
    // function
    auto end = mdl.find_first_of("> \n\r", p);
    if (end - p > 1)
    {
        string func = mdl.substr(p, end - p);
        std::transform(func.begin(), func.end(), func.begin(), convert);
        os << nspace << "::" << func;
    }
    else
    {
        os << "nullptr";
        ++end;
    }
    return end;
}

class lower
{
public:
    char operator()(char c) { return tolower(c); }
};

size_t dump_props(size_t p, const string &mdl, const std::string &hdr, ostream &os)
{
    os << "{ ";
    // Starting at the opening paren.
    p = passws(p + 1, mdl);
    bool first = true;
    while (mdl[p] != ')')
    {
        if (!first)
            os << ", ";
        first = false;
        auto end = mdl.find_first_of(" \r\n", p);
        auto slot = mdl.substr(p, end - p);
        transform(slot.begin(), slot.end(), slot.begin(), convert);
        os << hdr << "(ksl_" << slot << ", ";
        p = passws(end, mdl);
        std::string prop;
        if (mdl[p] == '\"')
        {
            // It's a quote. Find the end quote.
            bool end_found = false;
            prop = "\"";
            end = p + 1;
            while (!end_found)
            {
                auto temp = mdl.find_first_of("\"\r\n", end);
                prop += mdl.substr(end, temp - end);
                // If this is a linefeed, replace it.
                if (mdl[temp] == '\n')
                    prop += "\\n";
                end = temp;
                if (mdl[end] == '\"' && mdl[end - 1] == '\\')
                {
                    // Not found yet (escaped quote..just add it to the string.)
                    prop += "\"";
                }
                else if (mdl[end] == '\"')
                {
                    end_found = true;
                }
                ++end;
            }
            prop += "\"";
        }
        else
        {
            end = mdl.find_first_of(" \n\r)", p);
            prop = mdl.substr(p, end - p);
        }

        // Convert this property.
        if (slot == "oglobal" || slot == "odesc1" || slot == "ocapac" || slot == "osize" ||
            slot == "odesco" || slot == "oread" || slot == "ofval" || slot == "otval" ||
            slot == "ostrength" || slot == "omatch" || slot == "rval")
        {
            if (prop[0] == ',')
            {
                // Hard-coded string.
                prop = prop.substr(1);
                transform(prop.begin(), prop.end(), prop.begin(), convert);
            }
            else if (prop[0] == '<' && prop[1] == '+')
            {
                prop = "std::vector<Bits>({ ";
                bool first = true;
                p = passws(p + 2, mdl);
                while (mdl[p] == ',')
                {
                    if (!first)
                        prop += ", ";
                    first = false;
                    end = mdl.find_first_of(" \n\r>", p);
                    string pr = mdl.substr(p + 1, end - p - 1);
                    transform(pr.begin(), pr.end(), pr.begin(), convert);
                    prop += pr;
                    p = end;
                    if (mdl[p] == '>')
                        ++p;
                    p = passws(p, mdl);
                }
                prop += " })";
                end = p;
            }
            // just dump it.
            os << prop;
        }
        else if (slot == "rglobal")
        {
            // These are stored as strings, which represent global objects.
            if (prop[0] == ',')
            {
                string temp = prop.substr(1);
                transform(temp.begin(), temp.end(), temp.begin(), convert);
                prop = "std::vector<Bits>({ "+temp+" })";
            }
            else if (prop[0] == '<' && prop[1] == '+')
            {
                prop = "std::vector<Bits>({ ";
                bool first = true;
                p = passws(p + 2, mdl);
                while (mdl[p] == ',')
                {
                    if (!first)
                        prop += ", ";
                    first = false;
                    end = mdl.find_first_of(" \n\r>", p);
                    string pr = mdl.substr(p + 1, end - p - 1);
                    transform(pr.begin(), pr.end(), pr.begin(), convert);
                    prop += pr;
                    p = end;
                    if (mdl[p] == '>')
                        ++p;
                    p = passws(p, mdl);
                }
                prop += " })";
                end = p;
            }
            os << prop;
        }
        else if (slot == "oactor")
        {
            // Actor can be player, master, or robot.
            if (prop[0] != ',')
                throw runtime_error("Invalid actor");
            prop = prop.substr(1);
            transform(prop.begin(), prop.end(), prop.begin(), convert);
            os << "oa_" << prop;
        }
        else if (slot == "ovtype")
        {
            if (prop[0] != ',')
                throw runtime_error("Invalid vehicle type");
            prop = prop.substr(1);
            transform(prop.begin(), prop.end(), prop.begin(), convert);
            os << prop;
        }
        else if (slot == "ofmsgs")
        {
            if (prop[0] != ',')
                throw runtime_error("Invalid melee type");
            prop = prop.substr(1);
            transform(prop.begin(), prop.end(), prop.begin(), convert);
            os << prop;
        }
        else if (slot == "olint")
        {
            if (prop[0] != '[')
                throw runtime_error("Invalid event");
            prop = prop.substr(1);

            os << "olint_t(" << prop << ", ";
            p = passws(end + 1, mdl);
            end = skiptows(p, mdl);
            prop = mdl.substr(p, end - p);
            if (prop[0] != '<')
                throw runtime_error("Invalid clock");
            bool enable = (prop == "<CLOCK-DISABLE") ? false: true;
            os << (enable ? "true" : "false") << ",";
            p = passws(end + 1, mdl);
            end = skiptows(p, mdl);
            p = passws(end + 1, mdl);
            end = skiptows(p, mdl);
            prop = mdl.substr(p, end - p);
            if (prop[0] != ',')
                throw runtime_error("Invalid event name");
            prop = prop.substr(1);
            std::transform(prop.begin(), prop.end(), prop.begin(), lower());
            os << prop << ",";

            p = passws(end + 1, mdl);
            end = mdl.find_first_not_of("0123456789", p);
            prop = mdl.substr(p, end - p);
            os << prop << ")";
            end = mdl.find_first_of(']', end) + 1;
        }
        else if (slot == "obverb")
        {
            // Currently there is only one of these, and it's empty.
            if (prop != "<>")
                throw runtime_error("Invalid verb prop");
            os << "nullptr";
            end = p + 2;
        }   
        else
        {
            _ASSERT(0);
        }
        os << ")";

        p = passws(end, mdl);
    }
    os << " }";
    return p + 1;
}

size_t dump_contents(size_t p, const string &mdl, ostream &os)
{
    // If we're at a ">", then we're at the end of the form.
    // This can be true in rooms.
    if (mdl[p] == '>')
    {
        os << "{ }";
        return p;
    }
    os << "{ ";
    bool first = true;
    // Assumes p is at the opening parenthesis.
    while ((p = mdl.find_first_of("<)", p)) != string::npos)
    {
        if (mdl[p] == ')')
            break;
        if (!first)
            os << ", ";
        first = false;
        // It's a <. Advance past the get-obj portion to the name.
        p = skiptows(p, mdl) + 1;
        auto end = mdl.find_first_of('>', p);
        auto obj = mdl.substr(p, end - p);
        os << obj;
    }
    os << "}";
    return p + 1;
}

void proc_obj(const string &mdl, bool global, ostream &os)
{
    auto t = std::time(nullptr);
    tm *tm = localtime(&t);
    os << "// Generated at " << std::put_time(tm, "%m-%d-%y %H:%M:%S") << endl;
	os << "#pragma once" << endl;

    if (!global)
    {
        os << "const ObjectDefinition objects[] = {" << endl;
    }
	else
	{
		os << "const GObjectDefinition gobjects[] = {" << endl;
	}

    string::size_type pos = 0;
    string hdr = global ? "<GOBJECT" : "<OBJECT";
    int i = 0;
    while ((pos = mdl.find(hdr, pos)) != string::npos)
    {
        ++i;
		os << "{";
        string::size_type p = pos + hdr.size();
        p = passws(p, mdl);
        if (global)
        {
            if (mdl[p] == '<' && mdl[p + 1] == '>')
            {
                // empty name. Just output default empty string.
                os << "numbits, ";
            }
            else
            {
                size_t endp = skiptows(p, mdl);
                string b = mdl.substr(p, endp - p);
                transform(b.begin(), b.end(), b.begin(), convert);
                os << b << ", ";
            }
        }

        // Move to the synonyms
        if (mdl.substr(p, 7) == "DWINDOW")
        {
            //cerr << "OK\n";
        }
        p = dump_bracket_list(p, mdl, os);
        // Do the adjectives
        p = dump_bracket_list(p, mdl, os);

        // Object description
        p = mdl.find('\"', p);
        auto end = mdl.find('\"', p + 1);
        os << mdl.substr(p, end - p + 1) << ", ";
        p = end + 1;
        p = add_bits(p, mdl, os);

        p = passws(p, mdl);

        if (mdl[p] != '>')
        {
            os << ", ";
            p = dump_func(p, mdl, "obj_funcs", os);
            p = passws(p, mdl);
            if (mdl[p] != '>')
            {
                // Another field available.
                os << ", ";
                // Contents
                p = mdl.find('(', p);
                p = dump_contents(p, mdl, os);
                os << ", ";

                p = passws(p, mdl);
                if (mdl[p] != '>')
                {
                    p = dump_props(p, mdl, "OP", os);
                }
            }
        }

        os << "}," << endl;
        pos += hdr.size();
        ++pos;
    }
    cout << "};" << endl;
}

int main(int argc, char *argv[])
{
    po::options_description desc("Options");
    po::variables_map vm;

    string filename;
    bool global, room, strings, string_header_only;

    desc.add_options()
        ("input_file,i", po::value<string>(&filename)->required(), "Input file")
        ("global,g", po::value<bool>(&global)->default_value(false), "Global objects?")
        ("room,r", po::value<bool>(&room)->default_value(false), "Do rooms instead of objects")
        ("strings,s", po::value<bool>(&strings)->default_value(false), "Parse string globals")
        ("headers,h", po::value<bool>(&string_header_only)->default_value(false), "Show headers only for strings")
        ;

    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    }
    catch (exception &e)
    {
        cerr << e.what() << endl;
        cerr << desc << endl;
        return 1;
    }

    FILE *f = fopen(filename.c_str(), "rb");
    int rv = 0;

    if (f != nullptr)
    {
        fseek(f, 0, SEEK_END);
        size_t sz = ftell(f);
        fseek(f, 0, SEEK_SET);
        vector<char> mdl(sz);
        fread(&mdl[0], sizeof(char), mdl.size(), f);
        string s(mdl.begin(), mdl.end());

        ostream &os = cout;
        if (room)
            proc_room(s, os);
        else if (strings)
            proc_strings(s, string_header_only, os);
        else
            proc_obj(s, global, os);

        fclose(f);
    }
    else
    {
        cerr << "Unable to open file\n";
        rv = 1;
    }
    return rv;
}

