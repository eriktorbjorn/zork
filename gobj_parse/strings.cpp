#include "stdafx.h"
#include "funcs.h"
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

void dump_string_array(size_t str, const string &mdl, std::ostream &os)
{
    bool first = true;
    // Assumes that str points to the first string.
    while (mdl[str] != ']')
    {
        if (!first)
            cout << ", ";
        first = false;
        str = dump_string(str, mdl, os);
        str = passws(str, mdl);
    }
}

void dump_strings(const string &hdr, const string &mdl, bool header_only, ostream &os)
{
    size_t pos = mdl.find(hdr);
    while (pos != string::npos)
    {
        auto name = passws(pos + hdr.size(), mdl);
        auto end = skiptows(name, mdl);
        auto flag_name = mdl.substr(name, end - name);
        auto str = passws(end, mdl);
        if (mdl[str] == '\"')
        {
            transform(flag_name.begin(), flag_name.end(), flag_name.begin(), convert);
            os << (header_only ? "extern " : "") << "const std::string " << flag_name;
            if (!header_only)
            {
                os << " = ";
                // It's a string
                dump_string(str, mdl, os);
            }
            os << ";\n";
        }
        else if (mdl[str] == '\'' && mdl[str + 1] == '[')
        {
            // array
            transform(flag_name.begin(), flag_name.end(), flag_name.begin(), convert);
            os << (header_only ? "extern " : "") << "const std::vector<std::string> " << flag_name;
            if (!header_only)
            {
                os << " = { ";
                str = passws(str + 2, mdl);
                dump_string_array(str, mdl, os);
                os << "}";
            }
            os << ";\n";
        }
        pos = mdl.find(hdr, pos + 1);
    }
}

void proc_strings(const string &mdl, bool header_only, ostream &os)
{
    if (header_only)
    {
        os << "#pragma once\n";
        os << "#include <vector>\n";
        os << "#include <string>\n";
    }
    else
        os << "#include \"zstring.h\"" << endl;
    dump_strings("<PSETG ", mdl, header_only, os);
    dump_strings("<SETG ", mdl, header_only, os);
}