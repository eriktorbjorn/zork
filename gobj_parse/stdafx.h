// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include <assert.h>

#ifdef _MSC_VER
#include <crtdbg.h>
#endif

#ifndef _ASSERT
#define _ASSERT assert
#endif
